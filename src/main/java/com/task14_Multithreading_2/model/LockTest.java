package com.task14_Multithreading_2.model;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

public class LockTest {

  private int num = 0;
  private final static MyLock lock = new MyLock();

    //new Thread(this::print1).start();
    //new Thread(this::print2).start();
    //new Thread(this::print3).start();
    class MyThread extends Thread{
    @Override
      public void run(){
      try {
        lock.tryLock(3000, TimeUnit.MILLISECONDS);
        sleep(3000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      try {
          System.out.println(10);
        }finally {
          lock.unlock();
        }
      }
    }
  public void start() throws InterruptedException {
    MyThread myThread1 = new MyThread();
    MyThread myThread2 = new MyThread();
    MyThread myThread3 = new MyThread();
    myThread1.start();
    myThread2.start();
    myThread3.start();
    try {
      myThread1.join();myThread2.join();myThread3.join();
    }catch (InterruptedException e){}
  }


  public void print1() throws InterruptedException {
      lock.lockInterruptibly();
      try {
        Thread.sleep(2000);
        System.out.println(num + 10);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }finally {
        lock.unlock();
      }
  }

  public void print2() {
    lock.lock();
    try {
      Thread.sleep(2000);
      System.out.println(num + 20);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }finally {
      lock.unlock();
    }
  }

  public void print3() {
    lock.lock();
    try {
      Thread.sleep(2000);
      System.out.println(num + 30);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }finally {
      lock.unlock();
    } }
}
