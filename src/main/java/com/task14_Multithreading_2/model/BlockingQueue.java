package com.task14_Multithreading_2.model;

import java.util.concurrent.PriorityBlockingQueue;

public class BlockingQueue {
  private static java.util.concurrent.BlockingQueue<Character> queue = new PriorityBlockingQueue<Character>();
  private static volatile String data;
  private static int size;


  public BlockingQueue() {
    start();
  }

  public void start() {
    new Thread(() -> {
      char[] array = data.toCharArray();
      for (Character c : array) {
        try {
          Thread.sleep(100);
          queue.add(c);
        } catch (InterruptedException e) {
        }
      }
    }).start();
    new Thread(() -> {
      try {
        while (queue.isEmpty()) {
          Thread.sleep(100);
          Character character = queue.take();
          size++;
          if (size == data.length()) {
            break;
          }
        }
      } catch (InterruptedException e) {
      }
    }).start();
  }
}
