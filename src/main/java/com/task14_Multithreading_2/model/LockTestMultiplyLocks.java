package com.task14_Multithreading_2.model;

import java.util.concurrent.locks.ReentrantLock;

public class LockTestMultiplyLocks {

  private int num = 0;
  private final static ReentrantLock lock1 = new ReentrantLock();
  private final static ReentrantLock lock2 = new ReentrantLock();
  private final static ReentrantLock lock3 = new ReentrantLock();
  public void start() {
    new Thread(this::print1).start();
    new Thread(this::print2).start();
    new Thread(this::print3).start();

  }

  public void print1() {
    lock1.lock();
    try {
      Thread.sleep(2000);
      System.out.println(num + 10);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }finally {
      lock1.unlock();
    }

  }

  public void print2() {
    lock2.lock();
    try {
      Thread.sleep(2000);
      System.out.println(num + 20);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }finally {
      lock2.unlock();
    }

  }

  public void print3() {
    lock3.lock();
    try {
      Thread.sleep(2000);
      System.out.println(num + 30);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }finally {
      lock3.unlock();
    }

  }
}
