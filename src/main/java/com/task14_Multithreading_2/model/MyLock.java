package com.task14_Multithreading_2.model;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public class MyLock implements Lock {
  private int count;


  @Override
  public synchronized void lock() {
    if (count == 0) {
      count++;
    } else {
      try {
        wait();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

  @Override
  public synchronized void lockInterruptibly() throws InterruptedException {
    if (Thread.interrupted())
      throw new InterruptedException();
    if (count == 0) {
      count++;
    } else {
      try {
        wait();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

  @Override
  public synchronized boolean tryLock() {
    if (count == 0) {
      lock();
      return true;
    } else {
    return false;
    }
  }

  @Override
  public synchronized boolean tryLock(long time, TimeUnit unit) throws InterruptedException {
    if (count == 0) {
      lock();
      return true;
    } else {
      wait(unit.toMillis(time));
      if (count == 0){
        lock();
        return true;
      }else {
      return false;
      }
    }
  }

  @Override
  public synchronized void unlock() {
    count--;
    if (count == 0) {
      notify();
    }
  }

  @Override
  public Condition newCondition() {
    return null;
  }
}
